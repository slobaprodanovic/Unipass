<?php

namespace Drupal\unipass\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends ControllerBase{

  /**
   * URL: '/openid-connect/unipass/logout'
   *
   * Custom method.
   * Based on url parameters, performs logout of the user.
   * @return bool|\Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function externalLogout() {
    global $base_url;
    $data = $_POST;

    // Log this event.
    \Drupal::logger('unipass')->notice('JWT data posted on logout URL: <pre>@data</pre>',
      array(
        '@data' => print_r($data, TRUE),
      )
    );

    // Init.
    $messenger = \Drupal::messenger();

    // This shouldn't happen, just in case. Perform logout of the currently logged in user.
    if (\Drupal::currentUser()->isAnonymous() != TRUE) {
      module_load_include('pages.inc', 'user');
      user_logout();

      // Give user some feedback, redirect him to homepage.
      $messenger->addMessage(t('You are now logged out from Drupal, via SSO.'), $messenger::TYPE_STATUS);
      $response = new RedirectResponse($base_url);
      return $response->send();
    }
    // If there is no logged in user, probably this url is pinged with data of
    // user that needs to be logged out. If 'logout_token' is set, it contains
    // data about user to be logged out.
    elseif (isset($data['logout_token'])) {
      // Decode JWT data from logout_token.
      $decoded_data = $this->decodeIdToken($data['logout_token']);

      \Drupal::logger('unipass')->notice('Decoded data: <pre>@decoded_data</pre>',
        array('@decoded_data' => print_r($decoded_data, TRUE)));

      // @todo, After unipass is whitelisted, change client name to unipass.
      $loaded_user = $this->userLoadBySub($decoded_data['sub'], 'unipass');

      \Drupal::logger('unipass')->notice('Loaded user: <pre>@data</pre>',
        array('@data' => print_r($loaded_user, TRUE)));

      // There is no user with that data.
      if (($loaded_user == FALSE) || (!method_exists($loaded_user, 'id'))) {
        return FALSE;
      }

      // Destroy session (logout) of that specific user.
      \Drupal::service('session_manager')->delete($loaded_user->id());

      // Log this event.
      \Drupal::logger('unipass')->notice('User data that is logged out via SSO: <pre>@loaded_user</pre>',
        array('@loaded_user' => print_r($loaded_user, TRUE)));
    }

    // Just in case.
    return $this->redirect('<front>');
  }

  /**
   * Custom function.
   * Redirects user after logout data is received from SSO.
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   * URL: '/openid-connect/unipass/return'
   */
  public function redirectLogout() {
    global $base_url;

    $url = isset($_GET['state']) ? $_GET['state'] : NULL;

    \Drupal::logger('unipass')->notice('Data about logout received: @data',
      array(
        '@data' => $url,
      )
    );

    if ($url != NULL) {
      $state = isset($_SESSION['unipass']['redirect_state']) ? $_SESSION['unipass']['redirect_state'] : NULL;
      // Unset variable.
      unset($_SESSION['unipass']['redirect_state']);

      // Check if state corespondents with the state that is sent.
      if (($state == NULL) || $state == FALSE) {
        return $this->redirect($base_url);
      }

      if ($state == $url) {
        // Url is valid and the user may access it, redirect him.
        return $this->redirect($base_url . '/' . $url);
      }
    }

    // Just in case.
    return $this->redirect('<front>');
  }

  /**
   * Loads a user based on a sub-id and a login provider.
   */
  protected function userLoadBySub($sub, $client_name = 'unipass') {
    $query = \Drupal::database()->select('openid_connect_authmap', 'a');
    $query->addField('a', 'uid');
    $query->condition('a.sub', $sub);
    $results = $query->execute();
    $result = $results->fetchAll();

    $uid = isset($result[0]->uid) ? $result[0]->uid : FALSE;

    \Drupal::logger('unipass')->notice('SSSSData about logout received: <pre>@decoded_data</pre>',
      array('@decoded_data' => print_r($uid, TRUE)
      ));

    if ($uid != FALSE) {
      $account = \Drupal\user\Entity\User::load($uid);
      if (is_object($account)) {
        return $account;
      }

      return FALSE;
    }
  }

  /**
   * Custom function.
   * Performs decoding of $id_token.
   *
   * @id_token - token to be decoded.
   *
   * @return array
   */
  protected function decodeIdToken($id_token) {
    list($headerb64, $claims64, $signatureb64) = explode('.', $id_token);
    $claims64 = str_replace(array('-', '_'), array('+', '/'), $claims64);
    $claims64 = base64_decode($claims64);

    // Provide response.
    return json_decode($claims64, TRUE);
  }
}