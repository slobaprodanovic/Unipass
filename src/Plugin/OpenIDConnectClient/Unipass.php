<?php

namespace Drupal\unipass\Plugin\OpenIDConnectClient;

use Drupal\Core\Form\FormStateInterface;
use Drupal\openid_connect\Plugin\OpenIDConnectClientBase;

/**
 * Unipass OpenID Connect client.
 *
 * Used primarily to login to Drupal sites powered by oauth2_server or PHP
 * sites powered by oauth2-server-php.
 *
 * @OpenIDConnectClient(
 *   id = "unipass",
 *   label = @Translation("Unipass")
 * )
 */
class Unipass extends OpenIDConnectClientBase {
  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['authorization_endpoint'] = [
      '#title' => $this->t('Authorization endpoint'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['authorization_endpoint'],
    ];
    $form['token_endpoint'] = [
      '#title' => $this->t('Token endpoint'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['token_endpoint'],
    ];
    $form['userinfo_endpoint'] = [
      '#title' => $this->t('UserInfo endpoint'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['userinfo_endpoint'],
    ];
    $form['end_session_endpoint'] = array(
      '#title' => t('EndSession endpoint'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['end_session_endpoint'],
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoints() {
    return [
      'authorization' => $this->configuration['authorization_endpoint'],
      'token' => $this->configuration['token_endpoint'],
      'userinfo' => $this->configuration['userinfo_endpoint'],
      'end_session' => $this->configuration['end_session_endpoint'],
    ];
  }

  /**
   * End User Session in IdP.
   *
   * @param string $id_token
   *   A NON expired id_token (RECOMMENDED).
   * @param string $post_logout_redirect_url
   *   Logout url registered on the IdP.
   *
   * @see http://openid.net/specs/openid-connect-session-1_0-15.html
   * @see http://openid.net/specs/openid-connect-session-1_0-15.html#RPLogout
   */
  public function endSession($id_token, $post_logout_redirect_url = '') {
    global $base_url;

    $endpoints = $this->getEndpoints();
    \Drupal::logger('unipass')->warrning('Am I called?');

    // Temp fix @todo, remove later.
    if (($base_url == 'http://default/') || $base_url == 'http://default') {
      \Drupal::logger('unipass')->warrning('You must set $base_url value!');
      $base_url = 'https://openid.coworks.be/';
    }

    $url_options = array('query' => array(
      'id_token_hint' => $id_token,
      'post_logout_redirect_uri' => htmlentities($base_url . '/openid-connect/unipass/return')
      )
    );

    // Add state for redirect of user after logout.
    $state = $this->checkAnonymousAccessPermission();
    if ($state !== FALSE) {
      $url_options['query']['state'] = $state;
    }

    $path = \Drupal\Core\URL::fromUserInput($endpoints['end_session'], $url_options)->toString();
    $response = new RedirectResponse($path);

    \Drupal::logger('unipass')->notice('Data sent to logout API: <pre>@url_options</pre>',
      array(
        '@url_options' => print_r($url_options, TRUE)
      ));

    return $response->send();
  }

  /**
   * Performs check if anonymous user can access url that is provided
   * as parameter. If he can, $url parameter will be returned,
   * else FALSE will be returned.
   *
   * @param string $url - url to check
   *
   * @return string|boolean
   */
  public function checkAnonymousAccessPermission($url = NULL) {
    global $base_url;

    // Session variable for later check for session hijack.
    $_SESSION['unipass']['redirect_state'] = FALSE;

    // In HTTP_REFERER is stored info about page on which was user last
    // before click on logout button.
    if ($url == NULL) {
      $url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $base_url;
    }

    $site_url = $base_url;
    // Until $base_url is set on settings.php, use this fix.
    // @todo Remove later.
    if ($site_url == 'http://default') {
      $site_url = 'https://openid.coworks.be';
    }

    // Format url, remove site base url.
    $url = parse_url($url);
    $url = $url['path'];

    // If user was last on homepage, return him to homepage.
    if ($url == '/') {
      // Provide response.
      return FALSE;
    }

    // Finish with formatting url on which user was last.
    $pos = strpos($url, '/');
    if ($pos !== false) {
      $url = substr_replace($url, '', $pos, strlen('/'));
    }
    $redirect_url = $site_url;

    $_url = Url::fromInternalUri($url);
    $userCurrent = \Drupal::currentUser();
    if ($_url->access($userCurrent)) {
      // The user has access to the page in $path.
      $redirect_url = $site_url . '/' . $url;

      // Log this event.
      \Drupal::logger('unipass')->notice('After logout on SSO, user will
          be redirected to: @redirect_url', array('@redirect_url' => $redirect_url)
        );

      // Store data for later check, into session.
      $_SESSION['unipass']['redirect_state'] = $url;

      // Provide response.
      return $url;
    }

    // Log this event.
    \Drupal::logger('unipass')->notice('After logout on SSO, user will
          be redirected to: @redirect_url', array('@redirect_url' => $redirect_url)
    );

    // Provide response.
    return FALSE;
  }
}